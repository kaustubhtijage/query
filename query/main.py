# This is a sample Python script.
from fastapi import FastAPI
from fastapi import Depends
from sqlalchemy import inspect
from sqlalchemy.sql import text
from . import models
from .database import Session_local, engine, getDatafromDatabase
from .models import Book, BookID

app = FastAPI()

def get_db():
    db = Session_local()
    try:
        yield db
    finally:
        db.close

models.Base.metadata.create_all(bind=engine)

inspector = inspect(engine)
inspector.get_columns('Book')

@app.post("/insert_details")
async def add(book:Book, db: Session_local = Depends(get_db)):
    try:

        title = book.title
        primary_author = book.primary_author
        with engine.connect() as con:

            # data = (
            #          { "id": 3, "title": "The Alice", "primary_author": "Mark"},
            #          { "id": 4, "title": "The Wonderland ", "primary_author": "Alice"}
            #          )

            statement = text(f"""INSERT INTO Book(title, primary_author) VALUES("{title}","{primary_author}")""")
            print(statement)


            # for line in statement:
            con.execute(statement)


            db.commit()
            return {'Success': True}

    except Exception as err:
        import os,sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return {'Success': False, 'Message': str(err)}
    finally:
            con.close()

@app.post("/get_details")
async def add(db: Session_local = Depends(get_db)):
    try:
        with engine.connect() as con:

            rs = con.execute('SELECT * FROM Book')
            db.commit()

            for row in rs:
                print(row)
    except Exception as err:
        import os, sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return {'Success': False, 'Message': str(err)}

@app.post("/insert")
async def add(bookid:BookID,db: Session_local = Depends(get_db)):
    try:
        order_id = bookid.order_id
        price = bookid.price
        publication_id = bookid.publication_id
        with engine.connect() as con:

            # data = (
            #          { "order_id": 1,"price":450,"publication_id":7779990},
            #          {"order_id": 2, "price": 610, "publication_id": 555332},
            #          {"order_id": 3, "price": 921, "publication_id": 666555},
            #          {"order_id": 4, "price": 49, "publication_id": 8866774}
            #
            #          )

            statement = text(f"""INSERT INTO bookprofile(order_id, price, publication_id) VALUES("{order_id}","{price}","{publication_id}")""")
            print(statement)


            # for line in data:
            con.execute(statement)

            db.commit()
            return {'Success': True}

    except Exception as err:
        import os,sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return {'Success': False, 'Message': str(err)}

@app.post("/inner_join")
async def join(id:int , db: Session_local = Depends(get_db)):
    try:
        sqlquery = f''' SELECT * FROM book where id = {id}'''
        row = getDatafromDatabase(sqlquery)
        for r in row:
            footerquery = f'''select * from bookprofile where order_id ={r['id']}'''
            footer = getDatafromDatabase(footerquery)
            r['footer'] = footer
        db.commit()
        return {'Success': True, 'Message': row}
    except Exception as err:
        import os, sys
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        return {'Success': False, 'Message': str(err)}

