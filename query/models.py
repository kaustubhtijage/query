from pydantic import BaseModel
from sqlalchemy import Column, Integer, String, Float

from .database import Base

class DBBook(Base):
    __tablename__ = "Book"

    id = Column(Integer, primary_key =True, index =True)
    title = Column(String(40))
    primary_author = Column(String(45))


class Book(BaseModel):
   id:int
   title:str
   primary_author:str

class DBBookID(Base):
    __tablename__ = "bookprofile"

    order_id = Column(Integer, primary_key=True, index=True)
    price = Column(Integer)
    publication_id = Column(Integer)

class BookID(BaseModel):
    order_id:int
    price:int
    publication_id:int


class Config:
    orm_mode = True